package ru.baulina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-show-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by index.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX");
        @Nullable final Integer index = TerminalUtil.nexInt() -1;
        @Nullable final SessionDTO session = getSession();
        endpointLocator.getProjectEndpoint().removeOneProjectByIndex(session, index);
        System.out.println("[OK]");
        System.out.println();
    }

}
