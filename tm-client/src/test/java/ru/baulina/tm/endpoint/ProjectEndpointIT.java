package ru.baulina.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import ru.baulina.tm.IntegrationTest;

import java.util.ArrayList;
import java.util.List;

@Category(IntegrationTest.class)
public class ProjectEndpointIT {

//    @NotNull static private SessionDTO sessionTest;
//
//    @Rule
//    public final TestName testName = new TestName();
//
//    @NotNull
//    private static final SessionEndpointService sessionEndpointService = new SessionEndpointService();
//    @NotNull
//    private static final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
//    @NotNull
//    private static final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
//    @NotNull
//    private static final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();
//
//    @Before
//    public void setTestName() throws Exception {
//        System.out.println(testName.getMethodName());
//    }
//
//    @After
//    public void clearProject() throws Exception {
//        projectEndpoint.clearProjects(sessionTest);
//    }
//
//    @BeforeClass
//    public static void setSessions() throws Exception {
//        sessionTest = sessionEndpoint.openSession("test", "test");
//    }
//
//    @AfterClass
//    public static void closeSessions() throws Exception {
//        sessionEndpoint.closeSession(sessionTest);
//    }
//
//    @Test
//    public void testCreateProjectWithDescription() {
//        @Nullable ProjectDTO project = projectEndpoint.createProjectWithDescription(
//                sessionTest, "project", "description"
//        );
//        Assert.assertNotNull(project);
//        Assert.assertEquals("project", project.getName());
//        Assert.assertEquals("description", project.getDescription());
//    }
//
//    @Test
//    public void testFindAllProjects() {
//        @NotNull ProjectDTO project1 = projectEndpoint.createProject(sessionTest, "project1");
//        @NotNull ProjectDTO project2 = projectEndpoint.createProject(sessionTest, "project2");
//        @NotNull final List<ProjectDTO> listProjectExpected = projectEndpoint.findAllProjects(sessionTest);
//        Assert.assertEquals(2, listProjectExpected.size());
//        Assert.assertEquals(project1.getId(), listProjectExpected.get(0).getId());
//        Assert.assertEquals(project2.getId(), listProjectExpected.get(1).getId());
//    }
//
//    @Test
//    public void testFindOneProjectByIndex() {
//        @NotNull ProjectDTO project1 = projectEndpoint.createProject(sessionTest, "project1");
//        @NotNull ProjectDTO project2 = projectEndpoint.createProject(sessionTest, "project2");
//        Assert.assertEquals(project1.getId(), projectEndpoint.findOneProjectByIndex(sessionTest, 0).getId());
//        Assert.assertEquals(project2.getId(), projectEndpoint.findOneProjectByIndex(sessionTest, 1).getId());
//    }
//
//    @Test
//    public void testLoadProjects() {
//    }
//
//    @Test
//    public void testClearProjects() {
//        projectEndpoint.createProject(sessionTest, "project1");
//        projectEndpoint.createProject(sessionTest, "project2");
//        Assert.assertEquals(2, projectEndpoint.findAllProjects(sessionTest).size());
//        projectEndpoint.clearProjects(sessionTest);
//        Assert.assertEquals(0, projectEndpoint.findAllProjects(sessionTest).size());
//    }
//
//    @Test
//    public void testFindOneProjectById() {
//        @NotNull ProjectDTO project1 = projectEndpoint.createProject(sessionTest, "project1");
//        @NotNull final Long id1 = project1.getId();
//        @NotNull ProjectDTO project3 = projectEndpoint.findOneProjectById(sessionTest, id1);
//        @NotNull final Long id3 = project3.getId();
//        Assert.assertEquals(id1, id3);
//    }
//
//    @Test
//    public void testRemoveOneProjectById() {
//        projectEndpoint.createProject(sessionTest, "project1");
//        projectEndpoint.createProject(sessionTest, "project2");
//        @Nullable ProjectDTO project1 = projectEndpoint.findOneProjectByName(sessionTest, "project1");
//        @Nullable ProjectDTO project2 = projectEndpoint.findOneProjectByName(sessionTest, "project2");
//        Assert.assertNotNull(project1);
//        Assert.assertNotNull(project2);
//        @NotNull final Long id1 = project1.getId();
//        Assert.assertNotNull(projectEndpoint.findOneProjectById(sessionTest, id1));
//        projectEndpoint.removeOneProjectById(sessionTest, project1.getId());
//        Assert.assertNull(projectEndpoint.findOneProjectById(sessionTest, id1));
//        @NotNull final Long id2 = project2.getId();
//        Assert.assertNotNull(projectEndpoint.findOneProjectById(sessionTest, id2));
//        projectEndpoint.removeOneProjectById(sessionTest, project2.getId());
//        Assert.assertNull(projectEndpoint.findOneProjectById(sessionTest, id2));
//    }
//
//    @Test
//    public void testUpdateProjectById() {
//        projectEndpoint.createProject(sessionTest, "project");
//        @NotNull ProjectDTO project = projectEndpoint.findOneProjectByName(sessionTest, "project");
//        @NotNull final Long id = project.getId();
//        @NotNull ProjectDTO projectUpdate = projectEndpoint.updateProjectById(
//                sessionTest, id, "projectNew", "descriptionNew"
//        );
//        Assert.assertEquals("projectNew", projectUpdate.getName());
//        Assert.assertEquals("descriptionNew", projectUpdate.getDescription());
//    }
//
//    @Test
//    public void testUpdateProjectByIndex() {
//        projectEndpoint.createProject(sessionTest, "project");
//        @NotNull ProjectDTO project = projectEndpoint.updateProjectByIndex(
//                sessionTest, 0, "projectNew", "descriptionNew"
//        );
//        Assert.assertEquals("projectNew", project.getName());
//        Assert.assertEquals("descriptionNew", project.getDescription());
//    }
//
//    @Test
//    public void testRemoveProject() {
//        @Nullable ProjectDTO project1 = projectEndpoint.createProject(sessionTest, "project1");
//        @Nullable ProjectDTO project2 = projectEndpoint.createProject(sessionTest, "project2");
//        Assert.assertEquals(2, projectEndpoint.getProjectList().size());
//        projectEndpoint.removeProject(sessionTest, project1);
//        Assert.assertEquals(1, projectEndpoint.getProjectList().size());
//        projectEndpoint.removeProject(sessionTest, project2);
//        Assert.assertEquals(0, projectEndpoint.getProjectList().size());
//    }
//
//    @Test
//    public void testRemoveOneProjectByIndex() {
//        projectEndpoint.createProject(sessionTest, "project1");
//        projectEndpoint.createProject(sessionTest, "project2");
//        Assert.assertEquals(2, projectEndpoint.getProjectList().size());
//        projectEndpoint.removeOneProjectByIndex(sessionTest, 1);
//        Assert.assertEquals(1, projectEndpoint.getProjectList().size());
//        projectEndpoint.removeOneProjectByIndex(sessionTest, 0);
//        Assert.assertEquals(0, projectEndpoint.getProjectList().size());
//    }
//
//    @Test
//    public void testFindOneProjectByName() {
//        @NotNull ProjectDTO project1 = projectEndpoint.createProject(sessionTest, "project1");
//        @NotNull ProjectDTO project2 = projectEndpoint.createProject(sessionTest, "project2");
//        @NotNull String name1 = projectEndpoint.findOneProjectByName(sessionTest, "project1").getName();
//        @NotNull String name2 = projectEndpoint.findOneProjectByName(sessionTest, "project2").getName();
//        Assert.assertEquals(project1.getName(), name1);
//        Assert.assertEquals(project2.getName(), name2);
//    }
//
//    @Test
//    public void testCreateProject() {
//        Assert.assertEquals(0, projectEndpoint.getProjectList().size());
//        projectEndpoint.createProject(sessionTest, "projectNew");
//        Assert.assertEquals(1, projectEndpoint.getProjectList().size());
//    }
//
//    @Test
//    public void testRemoveOneProjectByName() {
//        projectEndpoint.createProject(sessionTest, "project1");
//        projectEndpoint.createProject(sessionTest, "project2");
//        Assert.assertEquals(2, projectEndpoint.getProjectList().size());
//        Assert.assertNotNull(projectEndpoint.findOneProjectByName(sessionTest,"project1"));
//        projectEndpoint.removeOneProjectByName(sessionTest, "project1");
//        Assert.assertNull(projectEndpoint.findOneProjectByName(sessionTest,"project1"));
//        Assert.assertEquals(1, projectEndpoint.getProjectList().size());
//        Assert.assertNotNull(projectEndpoint.findOneProjectByName(sessionTest,"project2"));
//        projectEndpoint.removeOneProjectByName(sessionTest, "project2");
//        Assert.assertEquals(0, projectEndpoint.getProjectList().size());
//    }
//
//    @Test
//    public void testGetProjectList() {
//        @NotNull ProjectDTO project1 = projectEndpoint.createProject(sessionTest, "project1");
//        @NotNull ProjectDTO project2 = projectEndpoint.createProject(sessionTest, "project2");
//        @Nullable final List<Long> expected = new ArrayList<>();
//        expected.add(project1.getId());
//        expected.add(project2.getId());
//        @NotNull final List<ProjectDTO> projects = projectEndpoint.getProjectList();
//        @NotNull final List<Long> actual = new ArrayList<>();
//        for (@NotNull final ProjectDTO project: projects) {
//            actual.add(project.getId());
//        }
//        Assert.assertEquals(expected, actual);
//    }

}