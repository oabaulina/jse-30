package ru.baulina.tm.exception.data;

public class DataLoadException extends RuntimeException {

    public DataLoadException(Throwable throwable) {
        super("Error! Problem with loading file...", throwable);
    }

}
