#!/bin/bash

APP_NAME="Task Manager"
NAME=task-manager

PID_FILE=$NAME.pid

echo "Для работы с $APP_NAME введите одну из следующих команд:";
echo "start - запуск сервера";
echo "stop - остановка сервера";
echo "status - статус";

case "$1" in
start)
        if [ -f $PID_FILE ]; then
            echo "$APP_NAME уже запущен"
        else
            mkdir -p log
            rm -f log/$NAME.log
            nohup java -jar ./$NAME.jar > log/$NAME.log 2>&1 &
            echo $! > $PID_FILE
            echo "$APP_NAME запущен"
        fi
;;
status)
        if [ -f $PID_FILE ]; then
            PID=`cat $PID_FILE`
            if [ -z "`ps axf | grep ${PID} | grep -v grep`" ]; then
                rm $PID_FILE
                echo "$APP_NAME не запущен"
            else
                echo "$APP_NAME запущен"
            fi
        else
            echo "$APP_NAME не запущен"
        fi
;;
stop)
        if [ -f $PID_FILE ]; then
        	PID=`cat $PID_FILE`
            echo "Завершение процесса с PID = $PID";
            kill -9 $PID
            rm $PID_FILE
        else
            echo "$APP_NAME не запущен"
        fi
;;

esac
